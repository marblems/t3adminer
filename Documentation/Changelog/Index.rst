.. include:: /Includes.rst.txt

.. _changelog:

.. converted from Markdown:
   pandoc --from markdown --to rst CHANGELOG.md -o CHANGELOG.rst

.. include:: ./CHANGELOG.rst
